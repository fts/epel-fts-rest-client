This repository is intended to host the [FTS-REST-Client][1] specfile until it receives approval for EPEL publishing.
In order to do that, a placeholder locaiton is needed where the specfile can be reviewed.

The goal is to publish the `fts-rest-client` package into EPEL.

#### Build the source tarball

```bash
$ git clone --depth=1 --branch v3.12.0-client https://gitlab.cern.ch/fts/fts-rest-flask.git fts-rest-client-3.12.0
$ tar -C fts-rest-client-3.12.0/ -czf fts-rest-client-3.12.0.tar.gz src/cli src/fts3 LICENSE setup.py setup.cfg --transform "s|^|fts-rest-client-3.12.0/|" --show-transformed-names
```

#### Build the SRPM

```bash
$ rpmbuild -bs fts-rest-client.spec --define "_sourcedir ${PWD}" --define "_srcrpmdir ${PWD}"
```

#### Build the RPM

```bash
$ rpmbuild --rebuild --define "_rpmdir ${PWD}/RPMS" fts-rest-client-3.12.0-1*.src.rpm
```

[1]: https://gitlab.cern.ch/fts/fts-rest-flask

